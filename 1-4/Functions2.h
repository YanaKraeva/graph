//
//  Functions2.h
//  Source
//
//  Created by MacBook Pro on 03.06.17.
//  Copyright © 2017 MacBook Pro. All rights reserved.
//

#ifndef Functions2_h
#define Functions2_h

#include <iostream>
#include <fstream>
#include <math.h>
#include <locale.h>
#include <vector>
#include <string>
#include <set>
#include <tuple>


using namespace std;

// вывод вектора целых чисел на экран
void OutVector(vector <int> a)
{
    for (int i=0;i<a.size();i++)
    {
        cout << a[i]<<", ";
    }
    cout << endl;
}

// вывод вектора троек целых чисел на экран
void OutVectorTuple(vector <tuple<int, int, int>> a)
{
    int a1, a2, a3;
    for (int i=0;i<a.size();i++)
    {
        tie(a1,a2,a3)=a[i];
        cout << " ("<<a1<<", "<<a2<<", "<<a3<<"), ";
    }
    cout << endl;
}


// нахождение ребра с минимальным весом во множестве ребер
void MinTupleInSet(set <tuple<int, int, int>> a,tuple<int, int, int> &w)
{
    tuple<int, int, int> b;
    int a1, a2, a3;
    int weigthmin;
    tie(a1,a2,a3)=(*a.begin());
    b=(*a.begin());
    weigthmin=a3;
    for(set <tuple<int, int, int>>::iterator i=a.begin();i!=a.end(); i++)
    {
        tie(a1,a2,a3)=(*i);
        if (weigthmin>a3)
        {
            b=(*i);
            weigthmin=a3;
        }
    }
    w=b;
}



// сортировка вектора троек по убыванию веса. наименьший элемент с наименьшим весом записан последним
void SortVectorTuple(vector <tuple<int, int, int>> &a)
{
    vector <tuple<int, int, int>> b;
    int a1, a2, a3;
    int weigthmax;
    int i1;
    b.clear();
    while (a.size()>0)
    {
        tie(a1,a2,a3)=a[0];
        weigthmax=a3;
        i1=0;
        for(int i=0;i<a.size();i++)
        {
            tie(a1,a2,a3)=a[i];
            if (weigthmax<a3)
            {
                i1=i;
                weigthmax=a3;
            }
        }
        b.push_back(a[i1]);
        a.erase(a.begin()+i1);
    }
    a.clear();
    for(int i=0;i<b.size();i++)
    {
        a.push_back(b[i]);
    }
}

#endif /* Functions2_h */
