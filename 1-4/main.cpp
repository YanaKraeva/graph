#include <iostream>
#include <fstream>
#include <math.h>
#include <locale.h>
#include <vector>
#include <string>
#include <set>
#include <tuple>
#include "Graph.h"

using namespace std;

void OutGraph(Graph a);

// main for 4 laba
int main()
{

    Graph a;
    
    int i1;
    vector<char> marks;
    vector<pair<int,int>> q23;
    cout<<endl;
    
    a.ReadGraph("/Users/macbookpro/Desktop/Source/Source/matr1.txt");
        OutGraph(a);
    i1 = a.CheckBipart(marks);
    q23 = a.GetMaximumMatchingBipart();
    if (i1 == 1)
    {
        cout<<"Граф является двудольным"<<endl;
    }
    else
    {
        cout<<"Граф не является двудольным "<<endl;
    }
    cout<<"Метки вершин "<<endl;
        OutVectorChar(marks);
    cout<<endl;
    cout<<"Алгоритм Куна поиска наибольшего паросочетания в графе, "<<endl;
    cout<<"Найдено паросочетание "<<endl;
        OutVectorPair(q23);
    
    return 0;
    
}



// main for laba3
int main1()
{
    Graph a;
    Graph b = Graph(10);
    bool circleExist;
    int i1;
    vector<int> q23;
    cout << endl;
    
    ofstream fout("/Users/macbookpro/Desktop/Source/Source/output1.txt", ios::out);
    
    a.ReadGraph("/Users/macbookpro/Desktop/Source/Source/matr1.txt");
        OutGraph(a);
    i1 = a.CheckEuler(circleExist);
    if (circleExist)
    {
        fout<<"Есть Эйлеров цикл"<<endl;
    }
    else
    {
        if (i1 > 0)
        {
            fout << "Нет Эйлерового цикла, но есть Эйлеров путь " << endl;
        }
        else
        {
            fout << "Нет Эйлерового цикла, и нет Эйлерового пути " << endl;
        }
    }
        fout<< "CheckEuler(circleExist)=" << i1 << " circleExist=" << circleExist << " " << endl;
    i1 = b.CheckEuler(circleExist);
        fout << endl;
        fout << "Для графа из изолированных вершин" << endl;
    if (circleExist)
    {
        fout << "Есть Эйлеров цикл" << endl;
    }
    else
    {
        if (i1 > 0)
        {
            fout << "Нет Эйлерового цикла,  но есть Эйлеров путь" << endl;
        }
        else
        {
            fout << "Нет Эйлерового цикла,  и нет Эйлерового пути" << endl;
        }
    }
    
    fout << "CheckEuler(circleExist)=" << i1 << " circleExist=" << circleExist << " " << endl;
    fout << endl;
    fout << "Алгоритм Флери " << endl;
    q23 = a.GetEuleraToirFleri();
    for (int i = 0; i < q23.size(); i++) {
        fout << q23[i] << " ";
    }
    
    OutVector(q23);
    fout << endl;
    fout << "Эффективный алгоритм поиска Эйлерового пути " << endl;
    q23 = a.GetEuleranTourEffective();
    
    for (int i = 0; i < q23.size(); i++) {
        fout << q23[i] << " ";
    }
    
    OutVector(q23);
    
    fout.close();
    
    return 0;
}

// main for laba2
int main4()
{
//    Graph a;
//    Graph b;
//    
//    cout << endl;
//    
//    a.ReadGraph("/Users/macbookpro/Desktop/Source/Source/matr2.txt");
//        OutGraph(a);
//    b = a.GetSpingTreePrima();
//        cout << endl;
//        cout << "Алгоритм Прима " << endl;
//        OutGraph(b);
//    b = a.GetSpaingTreeKruscal();
//        cout << endl;
//        cout << "Алгоритм Крускала " << endl;
//        OutGraph(b);
//    b = a.GetSpaingTreeBoruvka();
//        cout << endl;
//        cout << " Алгоритм Борувки " << endl;
//        OutGraph(b);
//    return 0;
    
    Graph g;
    g.ReadGraph("/Users/macbookpro/Desktop/Source/Source/matr1.txt");
    Graph gg=g.GetSpaingTreeBoruvka();
    //Graph gg = g.GetSpaingTreeKruscal();
    //Graph gg=g.GetSpaingTreePrima();
    gg.TransformToListOfEdges();
    gg.WriteGraph("/Users/macbookpro/Desktop/Source/Source/output1.txt");
    return 0;
    
}

// main for laba1
int main2()
{
    Graph a;
    
    a.ReadGraph("/Users/macbookpro/Desktop/Source/Source/matr1.txt");
        OutGraph(a);
        cout<<endl;
        cout<<"TransformToAdjList(),"<<endl;
    a.TransformToAdjList();
        OutGraph(a);
        cout<<endl;
        cout<<"AddEdge(1,2),"<<endl;
    a.AddEdge(1,2);
        OutGraph(a);
        cout<<endl;
        cout<<"RemoveEdge(2,5),"<<endl;
    a.RemoveEdge(2,5);
        OutGraph(a);
    a.WriteGraph("/Users/macbookpro/Desktop/Source/Source/output1.txt");
    
    a.ReadGraph("/Users/macbookpro/Desktop/Source/Source/matr2.txt");
        OutGraph(a);
        cout<<endl;
        cout<<"TransformToAdjList(),"<<endl;
    a.TransformToAdjList();
        OutGraph(a);
        cout<<endl;
    
        cout<<"AddEdge(1,2,23),"<<endl;
    a.AddEdge(1,2,23);
        OutGraph(a);
        cout<<endl;
        cout<<"ChangeEdge(1,2,25),"<<endl;
    cout<<"weigth="<<a.ChangeEdge(1,2,25)<<endl;
    OutGraph(a);
        cout<<endl;
        cout<<"RemoveEdge(2,5),"<<endl;
    a.RemoveEdge(2,4);
        OutGraph(a);
    a.WriteGraph("/Users/macbookpro/Desktop/Source/Source/output2.txt");
    
    
    a.ReadGraph("/Users/macbookpro/Desktop/Source/Source/output1.txt");
        OutGraph(a);
        cout<<endl;
        cout<<"TransformToListOfAdges(),"<<endl;
    a.TransformToListOfEdges();
        OutGraph(a);
        cout<<endl;
        cout<<"AddEdge(3,5),"<<endl;
    a.AddEdge(3,5);
        OutGraph(a);
        cout<<endl;
        cout<<"RemoveEdge(3,5),"<<endl;
    a.RemoveEdge(3,5);
        OutGraph(a);
    a.WriteGraph("/Users/macbookpro/Desktop/Source/Source/output3.txt");
    
    a.ReadGraph("/Users/macbookpro/Desktop/Source/Source/output2.txt");
        OutGraph(a);
        cout<<endl;
        cout<<"TransformToListOfAdges(),"<<endl;
    a.TransformToListOfEdges();
        OutGraph(a);
        cout<<endl;
        cout<<"AddEdge(3,5,10),"<<endl;
    a.AddEdge(3,5,10);
        OutGraph(a);
        cout<<endl;
        cout<<"ChangeEdge(3,5,15),"<<endl;
    cout<<"weigth="<<a.ChangeEdge(3,5,15)<<endl;
        OutGraph(a);
        cout<<endl;
        cout<<"RemoveEdge(3,5),"<<endl;
    a.RemoveEdge(3,5);
        OutGraph(a);
    a.WriteGraph("/Users/macbookpro/Desktop/Source/Source/output4.txt");
    
    
    a.ReadGraph("/Users/macbookpro/Desktop/Source/Source/output4.txt");
        OutGraph(a);
        cout<<endl;
        cout<<"TransformToAdjMatrix(),"<<endl;
    a.TransformToAdjMatrix();
        OutGraph(a);
        cout<<endl;
        cout<<"AddEdge(3,5,10),"<<endl;
    a.AddEdge(3,5,10);
        OutGraph(a);
        cout<<endl;
        cout<<"ChangeEdge(3,5,15),"<<endl;
    cout<<"weigth="<<a.ChangeEdge(3,5,15)<<endl;
        OutGraph(a);
        cout<<endl;
        cout<<"RemoveEdge(3,5),"<<endl;
    a.RemoveEdge(3,5);
        OutGraph(a);
    a.WriteGraph("/Users/macbookpro/Desktop/Source/Source/output5.txt");
    
    return 0;
}

/* Вывод информации о графе на экран  */
void OutGraph(Graph a)
{
    cout << "Граф " << endl;
    cout << "indicator=" << a.indicator << ", n=" << a.n << ", ";
    if (a.indicator=='e' || a.indicator=='E')
	   {
           cout << " m=" << a.m << ", ";
       }
    cout << endl;
    cout << "d=" << a.d << ",  w=" << a.w << ", " << endl;
    if (a.indicator == 'c' || a.indicator == 'C')
	   {
           OutMatrix(a.a);
       }
	   else
       {
           if (a.indicator == 'l' || a.indicator == 'L')
           {
               if (a.w == 0)
               {
                   OutVectorSetPair(a.b1);
               }
               else
               {
                   OutVectorSetTuple(a.b2);
               }
           }
           else
           {
               if (a.w == 0)
               {
                   OutSetPair(a.w1);
               }
               else
               {
                   OutSetTuple(a.w2);
               }
           }
       }
}


