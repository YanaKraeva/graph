#include <iostream>
#include <fstream>
#include <math.h>
#include <locale.h>
#include <vector>
#include <stack>
#include <string>
#include <set>
#include <tuple>



using namespace std;

void OutVectorChar(vector <char> a);
void OutVectorPair(vector < pair<int, int>> a);
vector <pair <int, int>> GetMaximumMatchingBipart2(int a, set<int> a1,
                                                   vector <pair <int, int>> k1,
                                                   vector <set<int>> b, 	vector<int> u);

/* рекурсивная функция для работы с проверкой графа на двудольность,
 функция возвращает 1, если граф может оказатся двудольным,
 функция возвращает 0, если граф не является двудольным,
 k - номер вершины,
 b - вектор множеств межных вершин,
 marks - вектор меток,
 s1 - метка для вершины k,
 s2 - метка для вершин, смежных с вершиной k
 */
int CheckBipart1(int k, vector <set<int>> &b, vector<char> &marks,char s1, char s2)
{
    set<int> w;
    int i1;
    int q;
    int u;
    u=1;
    marks[k-1]=s1;
    q=1;
    w.clear();
    for(set<int>::iterator i=b[k-1].begin();i!=b[k-1].end();i++)
    {
        i1=(*i);
        if (marks[i1-1]==' ')
        {
            marks[i1-1]=s2;
            w.insert(i1);
            b[i1-1].erase(k);
        }
        else
        {
            if (marks[i1-1]==s1)
            {
                u=0;
                q=0;
                break;
            }
        }
    }
    b[k-1].clear();
    if (q==1)
    {
        for(set<int>::iterator i=w.begin();i!=w.end();i++)
        {
            i1=(*i);
            u=CheckBipart1(i1,b,marks,s2,s1);
            if (u==0)
            {
                break;
            }
        }
    }
    return u;
    
}



/* рекурсивная подпрограмма для алгоритма Куна,
 a - множество вершин, которые не задействованы в паросочетании,
 k1 - паросочетание,
 b - вектор множеств смежных вершин,
 u - найденный путь
 */
vector <pair <int, int>> GetMaximumMatchingBipart1(set<int> a,
                                                   vector <pair <int, int>> k1,
                                                   vector <set<int>> b, 	vector<int> u)
{
    vector<pair<int,int>> q;
    vector<pair<int,int>> q1;
    int length;
    int i1;
    set<int> a1;
    length=k1.size();
    if (a.size()>1)
    {
        for (set<int>::iterator i=a.begin();i!=a.end();i++)
        {
            i1=(*i);
            a1.clear();
            a1=a;
            a1.erase(i1);
            q1=GetMaximumMatchingBipart2(i1,a1,k1,b,u);
            if (q1.size()>length)
            {
                q=q1;
                length=q.size();
            }
        }
    }
    else
    {
        q=k1;
    }
    return q;
    
}


/* рекурсивная подпрограмма для алгоритма Куна,
 a - вершина, из которой запускается поиск в ширину,
 a1- множество вершин, которые не задействованы в паросочетании
 и не содержат вершину a,
 k1 - паросочетание,
 b - вектор множеств смежных вершин,
 u - найденный путь
 */
vector <pair <int, int>> GetMaximumMatchingBipart2(int a, set<int> a1,
                                                   vector <pair <int, int>> k1,
                                                   vector <set<int>> b, 	vector<int> u)
{
    vector<pair<int,int>> q;
    vector <pair <int, int>> k2;
    
    vector <int> u1;
    vector<pair<int,int>> q1;
    set<int> a2;
    int i1, i2,i3;
    int n1;
    int length;
    length= int(k1.size());
    for (set<int>::iterator i=b[a-1].begin();i!=b[a-1].end();i++)
    {
        i1=(*i);
        if (i1==u[0] || i1==u[u.size()-1])
        {
            if (i1==u[0])
            {
                n1=u[u.size()-1];
            }
            else
            {
                n1=u[0];
            }
            for (set<int>::iterator j=b[n1-1].begin();j!=b[n1-1].end();j++)
            {
                i2=(*j);
                for (set<int>::iterator j1=a1.begin();j1!=a1.end();j1++)
                {
                    i3=(*j1);
                    if (i2==i3)
                    {
                        u1.clear();
                        u1.push_back(a);
                        for (int j2=0;j2<u.size();j2++)
                        {
                            u1.push_back(u[j2]);
                        }
                        u1.push_back(i2);
                        k2.clear();
                        for(int j2=0;j2<u1.size()-1;j2=j2+2)
                        {
                            k2.push_back(make_pair(u1[j2],u1[j2+1]));
                        }
                        a2.clear();
                        a2=a1;
                        a2.erase(i2);
                        q1.clear();
                        q1=GetMaximumMatchingBipart1(a2,k2,b,u1);
                        if (q1.size()>length)
                        {
                            q=q1;
                            length =  int(q.size());
                        }
                    }
                }
            }
        }
        
    }
    return q;
}





// вывод вектора символов на экран
void OutVectorChar(vector <char> a)
{
    ofstream fout("/Users/macbookpro/Desktop/Source/Source/output1.txt", ios::out);
    
    for (int i=0;i<a.size();i++)
    {
        cout << a[i] << ", ";
        fout << a[i] << " ";
    }
    cout << endl;
    
    fout.close();
}

// вывод вектора пар целых чисел на экран
void OutVectorPair(vector < pair<int, int>> a)
{
    ofstream fout("/Users/macbookpro/Desktop/Source/Source/output1.txt", ios::out);
    
    for (int i=0;i<a.size();i++)
    {
        cout << " ("<<a[i].first <<", "<<a[i].second<<"), ";
        fout << " ("<<a[i].first <<", "<<a[i].second<<") ";
    }
    cout<<endl;
    
    fout.close();
}





