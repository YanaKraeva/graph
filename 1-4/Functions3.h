//
//  Functions3.h
//  Source
//
//  Created by MacBook Pro on 04.06.17.
//  Copyright © 2017 MacBook Pro. All rights reserved.
//

#ifndef Functions3_h
#define Functions3_h

#include <iostream>
#include <fstream>
#include <math.h>
#include <locale.h>
#include <vector>
#include <stack>
#include <string>
#include <set>
#include <tuple>
#include "Dsu.h"


using namespace std;


// вывод множества целых чисел на экран
void OutSet(set <int> a)
{
    for(set < int>::iterator i = a.begin();
        i != a.end(); i++)
    {
      		cout << (*i) <<", ";
    }
    cout<<endl;
    
}

// вывод вектора множеств целых чисел на экран
void OutVectorSet(vector < set <int> > a)
{
    for (int i=0;i<a.size();i++)
    {
        cout << i+1<<":  ";
        for(set <int>::iterator j = a[i].begin();
            j != a[i].end(); j++)
        {
            cout << " "<<(*j) <<", ";
        }
        cout<<endl;
    }
}


/* проверка, является ли ребро мостом,
 функция возвращает 1, если ребро между двумя данными вершинами
 не является мостом,
 функция возвращает 0, если ребро между двумя данными вершинами
 не является мостом
 */
int Most(vector <set <int>> a, int i1, int j1)
{
    Dsu b = Dsu(int(a.size()));
   // int i2;
    int q;
    q = 1;
    a[i1-1].erase(j1);
    a[j1-1].erase(i1);
    for (int i = 0; i < a.size(); i++)
    {
        for (set<int>::iterator j = a[i].begin(); j != a[i].end(); j++)
        {
            b.Union(i+1,(*j));
        }
    }
    for (int i = 1; i < a.size(); i++)
    {
        if (b.Find(i) != b.Find(i+1))
        {
            q = 0;
            break;
        }
    }
    a[i1-1].insert(j1);
    a[j1-1].insert(i1);
    return q;
}

#endif /* Functions3_h */
