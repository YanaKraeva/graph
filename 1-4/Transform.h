#include <iostream>
#include <fstream>
#include <math.h>
#include <locale.h>
#include <vector>
#include <string>
#include <set>
#include <tuple>

using namespace std;

//подпрограммы для выполнения преобразования одних способов представления графа в другие


// преобразование из матрицы смежности в список смежности для невзвешенного графа
void TransformMatrixToList1(vector<vector<int>> &a, vector<set<pair<int,int>>> &b)
{
    set <pair<int, int>> b1;
    b.clear();
    b1.clear();
    for (int i=0;i<a.size();i++)
    {
        b1.clear();
        for (int j=0;j<a[i].size();j++)
        {
            if (a[i][j]>0)
            {
                b1.insert(make_pair(i+1,j+1));
            }
        }
        b.push_back(b1);
    }
    a.clear();
}


// преобразование из матрицы смежности в список смежности для взвешенного графа
void TransformMatrixToList2(vector<vector<int>> &a, vector<set<tuple<int,int,int>>> &b)
{
    set<tuple<int,int,int>> b1;
    b.clear();
    b1.clear();
    for (int i = 0; i < a.size(); i++)
    {
        b1.clear();
        for (int j = 0; j < a[i].size(); j++)
        {
            if (a[i][j] > 0)
            {
                b1.insert(make_tuple(i+1, j+1, a[i][j]));
            }
        }
        b.push_back(b1);
    }
    a.clear();
}


// преобразование из матрицы смежности в список ребер для невзвешенного графа
void TransformMatrixToListOfEdges1(int &m, vector<vector<int>> &a, set<pair<int,int>> &b)
{
    b.clear();
    m = 0;
    for (int i = 0; i < a.size(); i++)
    {
        for (int j = 0; j < a[i].size(); j++)
        {
            if (a[i][j] > 0)
            {
                m++;
                b.insert(make_pair(i+1, j+1));
            }
        }
    }
    a.clear();
}

// преобразование из матрицы смежности в список ребер для взвешенного графа
void TransformMatrixToListOfEdges2(int &m, vector<vector<int>> &a, set<tuple<int,int,int>> &b)
{
    b.clear();
    m = 0;
    for (int i = 0; i < a.size(); i++)
    {
        for (int j = 0; j < a[i].size(); j++)
        {
            if (a[i][j] > 0)
            {
                m++;
                b.insert(make_tuple(i+1, j+1, a[i][j]));
            }
        }
    }
    a.clear();
}


// преобразование из списка смежности в матрицу смежности для невзвешенного графа
void TransformListToMatrix1(vector<set<pair<int,int>>> &a, vector<vector<int>> &b)
{
    vector<int> b1;
    b.clear();
    b1.clear();
    for (int i = 0; i < a.size(); i++)
    {
        b1.clear();
        for (int j = 0; j < a.size(); j++)
        {
            b1.push_back(0);
        }
        for (set<pair<int,int>>::iterator j = a[i].begin(); j != a[i].end(); j++)
        {
            b1[(*j).second-1] = 1;
        }
        b.push_back(b1);
    }
    a.clear();
}


// преобразование из списка смежности в матрицу смежности для взвешенного графа
void TransformListToMatrix2(vector<set<tuple<int,int,int>>> &a, vector<vector<int>> &b)
{
    vector<int> b1;
    int a1, a2,a3;
    b.clear();
    b1.clear();
    for (int i = 0; i < a.size(); i++)
    {
        b1.clear();
        for (int j = 0; j < a.size(); j++)
        {
            b1.push_back(0);
        }
        for( set<tuple<int,int,int>>::iterator j = a[i].begin(); j != a[i].end(); j++)
        {
            tie(a1,a2,a3) = (*j);
            b1[a2-1] = a3;
        }
        b.push_back(b1);
    }
    a.clear();
}


// преобразование из списка ребер в матрицу смежности для невзвешенного графа
void TransformListOfEdgesToMatrix1(int n, set<pair<int,int>> &a, vector<vector<int>> &b , int d)
{
    vector<int> b1;
    b.clear();
    b1.clear();
    for (int i = 0; i < n; i++)
    {
        b1.clear();
        for (int j = 0; j < n; j++)
        {
            b1.push_back(0);
        }
        b.push_back(b1);
    }
    for (set<pair<int,int>>::iterator j = a.begin(); j != a.end(); j++)
    {
        b[(*j).first-1][(*j).second-1] = 1;
        if (d == 0)
            b[(*j).second-1][(*j).first-1] = 1;
    }
    a.clear();
}


// преобразование из списка ребер в матрицу смежности для взвешенного графа
void TransformListOfEdgesToMatrix2(int n, set<tuple<int,int,int>> &a, vector<vector<int>> &b, int d)
{
    vector<int> b1;
    int a1, a2,a3;
    b.clear();
    b1.clear();
    for (int i=0;i<n;i++)
    {
        b1.clear();
        for (int j = 0; j < n; j++)
            b1.push_back(0);
        b.push_back(b1);
    }
    for (set<tuple<int,int,int>>::iterator j = a.begin(); j != a.end(); j++)
    {
        
        tie(a1,a2,a3) = (*j);
        b[a1-1][a2-1] = a3;
        if (d == 0)
            b[a2-1][a1-1] = a3;
    }
    a.clear();
}


// преобразование из списка смежности в список ребер для невзвешенного графа
void TransformListToListOfEdges1(int &m,vector < set <pair<int, int>>> &a, set <pair<int, int>> &b )
{
    b.clear();
    m = 0;
    for (int i = 0; i < a.size(); i++)
    {
        for (set<pair<int,int>>::iterator j = a[i].begin(); j != a[i].end(); j++)
        {
            m++;
            b.insert((*j));
        }
    }
    a.clear();
}


// преобразование из списка смежности в список ребер для взвешенного графа
void TransformListToListOfEdges2(int &m,vector <set <tuple<int, int, int>>> &a, set <tuple<int, int, int>> &b)
{
    b.clear();
    m = 0;
    for (int i = 0; i < a.size(); i++)
    {
        for (set<tuple<int,int,int>>::iterator j = a[i].begin(); j != a[i].end(); j++)
        {
            m++;
            b.insert((*j));
        }
    }
    a.clear();
}


// преобразование из списка ребер в список смежности для невзвешенного графа
void TransformListOfEdgesToList1(int n, set<pair<int,int>> &a, vector <set<pair<int,int>>> &b)
{
    set<pair<int,int>> b1;
    b.clear();
    b1.clear();
    for (int i = 0; i < n; i++)
        b.push_back(b1);
    for (set<pair<int,int>>::iterator j=a.begin(); j != a.end(); j++)
        b[(*j).first-1].insert((*j));
    a.clear();
}


// преобразование из списка ребер в список смежности для взвешенного графа
void TransformListOfEdgesToList2(int n, set<tuple<int,int,int>> &a, vector<set<tuple<int,int,int>>> &b)
{
    int a1, a2,a3;
    set<tuple<int,int,int>> b1;
    b.clear();
    b1.clear();
    for (int i = 0; i < n; i++)
        b.push_back(b1);
    for (set<tuple<int,int,int>>::iterator j = a.begin(); j != a.end(); j++)
    {
        tie(a1, a2, a3) = (*j);
        b[a1-1].insert((*j));
    }
    a.clear();
}
