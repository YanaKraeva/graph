#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>
#include <locale.h>
#include <vector>
#include <string>
#include <set>
#include <tuple>

using namespace std;

/* подпрограммы  для работы с преобразованием вектора строк в разные структуры для представления графа, и для вывода информации на экран */

/*  cоздание множества пар структуры для списка ребер для невзвешенного графа на основе массива строк  */
void StringToListOfEdges1(vector <string> st, set <pair<int, int>> &a)
{
    int from = 0, to = 0;
    for (int i = 0; i < st.size(); i++) {
        istringstream strst(st[i]);
        while (strst >> from >> to)
            a.insert(make_pair(from, to));
    }
}


/*  cоздание множества троек структуры для списка ребер для взвешенного графа на основе массива строк  */
void StringToListOfEdges2(vector <string> st, set <tuple<int,int,int>> &a)
{
    int from = 0, to = 0, weight = 0;
    for (int i = 0; i < st.size(); i++) {
        istringstream strst(st[i]);
        while (strst >> from >> to >> weight)
            a.insert(make_tuple(from, to, weight));
    }
}


/*  cоздание вектора множества пар структуры для списка смежности для невзвешенного графа на основе массива строк */
void StringToList1(vector <string> st, vector < set <pair<int, int>>> &a)
{
    a.resize(st.size());
    int edge;
    for (int i = 0; i < st.size(); i++) {
        istringstream strst(st[i]);
        while (strst >> edge)
            a[i].insert(make_pair(i+1, edge));
    }
}


//  cоздание вектора множества троек структуры для списка смежности для взвешенного графа на основе массива строк
void StringToList2(vector <string> st, vector<set<tuple<int, int, int>>> &a)
{
    a.resize(st.size());
    int edge = 0, weight = 0;
    for (int i = 0; i < st.size(); i++) {
        istringstream strst(st[i]);
        while (strst >> edge >> weight)
            a[i].insert(make_tuple(i+1, edge, weight)); 
    }

}


/*  cоздание матрицы на основе массива строк */
void StringToMatrix(vector <string> st, vector<vector <int>> &a)
{
    a.resize(st.size());
    int vertice;
    for (int i = 0; i < st.size(); i++)
    {
        istringstream strst(st[i]);
        a[i].resize(st.size());
        int j = 0;
        while (strst >> vertice) {
            a[i][j] = vertice;
            j++;
        }
    }
}


/* вывод матрицы на экран */
void OutMatrix(vector <vector <int>> a)
{
    for (int i = 0; i < a.size(); i++)
    {
        for (int j = 0; j < a[i].size(); j++)
            cout << a[i][j] << ", ";
        cout << endl;
    }
}


/* вывод вектора множеств пар на экран, */
void OutVectorSetPair(vector <set<pair<int,int>>> a)
{
    for (int i=0;i<a.size();i++)
    {
        cout << i << ":  ";
        for(set<pair<int,int>>::iterator j = a[i].begin(); j != a[i].end(); j++)
            cout << " (" << (*j).first << ", " << (*j).second << ") ";
        cout<<endl;
    }
}


/* вывод вектора множеств троек на экран */
void OutVectorSetTuple(vector < set<tuple<int,int,int>>> a)
{
    int a1, a2, a3;
    for (int i = 0 ; i < a.size(); i++)
    {
        cout << i << ":  ";
        for(set<tuple<int,int,int>>::iterator j = a[i].begin(); j != a[i].end(); j++)
        {
            tie(a1, a2, a3)=(*j);
            cout << " (" << a1 << ", " << a2 << ", " << a3 << ") ";
        }
        cout<<endl;
    }
}


/* вывод множества пар на экран */
void OutSetPair(set<pair<int, int>> a)
{
    for (set<pair<int,int>>::iterator i = a.begin(); i != a.end(); i++)
    {
      		cout << " (" << (*i).first << ", " << (*i).second << ") " << endl;
    }
}


/* вывод множества троек на экран */
void OutSetTuple(set<tuple<int,int,int>> a)
{   
    int a1, a2, a3;
    for(set<tuple<int,int,int>>::iterator i = a.begin(); i != a.end(); i++)
    {
        tie(a1, a2, a3) = (*i);
        cout << " (" << a1 << ", " << a2 << ", " << a3 << ") " << endl;
      		
    }
}
