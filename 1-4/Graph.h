#include <iostream>
#include <fstream>
#include <math.h>
#include <locale.h>
#include <vector>
#include <string>
#include <set>
#include <tuple>
#include "Init.h"
#include "Transform.h"
#include "Functions2.h"
#include "Functions3.h"
#include "Functions4.h"
#include "Dsu.h"

using namespace std;

// класс для  работы  с графом

class Graph
{
public:
    char indicator; // индикатор представления графа
    int n; // количество вершин графа
    int m; // количество ребер графа для представления графа в виде списка ребер
    int d, w; // индикаторы является ли граф ориентированным, является ли граф взвешенным
    
    vector<vector<int>> a; // матрица смежности
    vector<set<pair<int,int>>> b1; // список смежных вершин для невзвешенного графа
    vector<set<tuple<int,int,int>>> b2; // список смежных вершин для взвешенного графа
    set<pair<int,int>> w1; // список ребер для невзвешенного графа
    set<tuple<int,int,int>> w2; // список ребер для взвешенного графа
    
    // конструктор без параметров
    Graph()
    {
    }
    
    // конструктор взвешенного ориентированного графа с N изолированными вершинами
    Graph(int N)
    {
        vector <int> a1;
        indicator = 'C';
        n = N;
        m = 0;
        d = 0;
        w = 1;
        a.clear();
        a1.clear();
        for (int i = 0; i < n; i++)
        {
            a1.clear();
            for(int j = 0; j < n; j++)
            {
                a1.push_back(0);
            }
            a.push_back(a1);
        }
    }
    
    void TransformToAdjMatrix(); // преобразование представления графа в матрицу смежности
    void TransformToAdjList();  // преобразование представления графа в список смежных вершин
    void TransformToListOfEdges(); // преобразование представления графа в список ребер
    int ChangeEdge(int from, int to, int newWeight); // изменение веса ребра, функция возвращает старое значение веса ребра для взвешенного графа, и функция возвращает 0 для невзвешенного графа
    void RemoveEdge(int from, int to);  // удаление ребра
    void AddEdge(int from, int to, int weight = 1); //добавление нового ребра
    void WriteGraph(string fileName); //запись графа в файл
    void ReadGraph(string fileName); //чтение графа из файла
    
    /* проверка графа на двудольность,
     функция возвращает 1, если граф двудольный,
     функция возвращает 0, если граф не является двудольным
     */
    int CheckBipart(vector<char> &marks)
    {
        vector <set<int>> b;
        set<int> h;
        int k;
        int i1;
        //char s1;
        int u;
        u=1;
        char indicator1;
        indicator1=indicator;
        TransformToAdjMatrix();
        h.clear();
        for(int i=0;i<a.size();i++)
        {
            b.push_back(h);
        }
        for(int i=0;i<a.size();i++)
        {
            marks.push_back(' ');
            for (int j=0;j<a.size();j++)
            {
                if (a[i][j]>0)
	             		{
                            b[i].insert(j+1);
                            b[j].insert(i+1);
                        }
            }
        }
        k=0;
        i1=1;
        while (i1==1)
        {
            k=0;
            for(int i=0;i<n;i++)
            {
                if (b[i].size()>0 )
                {
                    k=i+1;
                    break;
                }
            }
            
            if (k>0)
            {
                u=CheckBipart1(k,b,marks,'A','B');
                
                if (u==0)
                {
                    i1=0;
                    break;
                }
            }
            else
            {
                u=1;
                i1=0;
            }
        }
        
        if (indicator1=='l' || indicator1=='L')
        {
            TransformToAdjList();
        }
        if (indicator1=='e' || indicator1=='E')
        {
            TransformToListOfEdges();
        }
        return u;
        
    }
    
    // алгоритм Куна нахождения наибольшего паросочетания
    vector <pair <int, int>> GetMaximumMatchingBipart()
    {
        vector<pair<int,int>> q;
        vector <set<int>> b;
        set<int> h;
        // множество вершин, которое не учавствует в данном паросочетании
        set<int> alpha;
        // множество всех ребер графа, без повторений
        vector <pair<int,int>> a1;
        vector<pair<int,int>> q1;
        vector<pair<int,int>> q2;
        vector <int> u;
        int length;
        int n1;
        char indicator1;
        indicator1=indicator;
        TransformToAdjMatrix();
        n1=floor(n/2);
        h.clear();
        for(int i=0;i<a.size();i++)
        {
            b.push_back(h);
        }
        a1.clear();
        alpha.clear();
        for(int i=0;i<a.size();i++)
        {
            for (int j=0;j<a.size();j++)
            {
                if (a[i][j]>0)
	             		{
                            b[i].insert(j+1);
                            b[j].insert(i+1);
                        }
            }
        }
        
        for(int i=0;i<a.size();i++)
        {
            alpha.insert(i+1);
            for (int j=i;j<a.size();j++)
            {
                if (a[i][j]>0)
	             		{
                            a1.push_back(make_pair(i+1,j+1));
                        }
            }
        }
        
        q.clear();
        q1.clear();
        length=0;
        for (int i=0;i<a1.size();i++)
        {
            alpha.erase(a1[i].first);
            alpha.erase(a1[i].second);
            q2.clear();
            q2.push_back(a1[i]);
            u.clear();
            u.push_back(a1[i].first);
            u.push_back(a1[i].second);
            q1=GetMaximumMatchingBipart1(alpha,q2,b,	u);
            alpha.insert(a1[i].first);
            alpha.insert(a1[i].second);
            if (q1.size()>length)
            {
                q=q1;
                length=int(q.size());
            }
            if (length==n1)
            {
                break;
            }
            
        }
        
        
        if (indicator1=='l' || indicator1=='L')
        {
            TransformToAdjList();
        }
        if (indicator1=='e' || indicator1=='E')
        {
            TransformToListOfEdges();
        }
        return q;
    }
    
    // алгоритм Прима, для корректной работы алгоритма Прима нужно, чтобы не было ребер с нулевым весом
    Graph GetSpaingTreePrima()
    {
        vector<int> a1;
        vector<int> a2;
        int weigthmin;
        Graph alpha = Graph(n);
        vector<vector<int>> b;
        int i1, j1;
        char indicator1;
        indicator1 = indicator;
        TransformToAdjMatrix();
        b = a;
        a1.push_back(1);
        for (int i = 1; i < n; i++)
        {
            a2.push_back(i+1);
        }
        for (int t = 0; t < n-1; t++)
        {
            weigthmin=0;
            i1 =-1;
            j1 =-1;
            for (int i = 0; i < a1.size(); i++)
            {
                for (int j = 0; j < a2.size(); j++)
                {
                    if ((b[a1[i]-1][a2[j]-1]>0 && weigthmin > b[a1[i]-1][a2[j]-1]) ||
                        (b[a1[i]-1][a2[j]-1]>0 && weigthmin == 0) )
                    {
                        weigthmin = b[a1[i]-1][a2[j]-1];
                        i1 = i;
                        j1 = j;
                        
                    }
                }
            }
            b[a1[i1]-1][a2[j1]-1]=0;
            b[a2[j1]-1][a1[i1]-1]=0;
            alpha.AddEdge(a1[i1], a2[j1], weigthmin);
            a1.push_back(a2[j1]);
            a2.erase(a2.begin()+j1);
            
        }
        if (indicator1 == 'L')
        {
            TransformToAdjList();
        }
        if (indicator1 == 'E')
        {
            TransformToListOfEdges();
        }
        return alpha;
    }
    
    /* Проверка существования Эйлерова пути и цикла в графе,
     функция возвращает вершину, с которой можно начинать
     построение Эйлерова пути или цикла,
     и функция возвращает 0, если ни Эйлерова пути, ни Эйлерова цикла
     в данном графе нет,
     если граф состоит из
     
     изолированных вершин,
     то функция возвращает 1,
     circleExist = true - Эйлеров цикл есть,
     circleExist=false - Эйлерова цикла нет
     */
    int CheckEuler(bool &circleExist)
    {
        vector<int> step;
        Dsu b = Dsu(n);
        // Вершины, которые имеют связи
        set <int> a1;
        int i1, i2, i3;
        int u;
        char indicator1;
        indicator1 = indicator;
        TransformToAdjMatrix();
        
        u = 1;
        a1.clear();
        for (int i = 0; i < n; i++)
        {
            i1 = 0;
            for (int j = 0; j < n; j++)
            {
                if (a[i][j] > 0)
                {
                    i1++;
                    a1.insert(i+1);
                    a1.insert(j+1);
                    b.Union(i+1, j+1);
                }
            }
            step.push_back(i1);
        }
        circleExist = true;
        u = 0;
        i2 = 0;
        for (int i = 0; i < n; i++)
        {
            if (step[i] % 2 == 1)
            {
                i2++;
                if (u == 0)
                {
                    u = i+1;
                    circleExist = false;
                }
            }
        }
        if (i2 == 0)
        {
            for (int i = 0; i < step.size(); i++)
            {
                if (step[i] > 0)
                {
                    u = i+1;
                    break;
                }
            }
            if (u == 0)
            {
                u = 1;
            }
        }
        
        if (i2 > 2)
        {
            u = 0;
        }
        if (a1.size() > 0)
        {
            i3 = b.Find((*a1.begin()));
            for (set<int>::iterator i = a1.begin(); i != a1.end(); i++)
            {
                if (i3 != b.Find((*i)))
                {
                    u = 0;
                    circleExist = false;
                    break;
                }
            }
        }
        if (indicator1 == 'L')
        {
            TransformToAdjList();
        }
        if (indicator1 == 'E')
        {
            TransformToListOfEdges();
        }
        return u;
        
    }
    
    //  Алгоритм  Флери
    vector<int> GetEuleraToirFleri()
    {
        vector<set<int>> b;
        vector<int> q23;
        set<int> h;
        bool circleExist;
        int i1;
        int u,v;
        char indicator1;
        indicator1 = indicator;
        TransformToAdjMatrix();
        
        q23.clear();
        i1 = CheckEuler(circleExist);
        if (i1 > 0)
        {
            m = 0;
            h.clear();
            for (int i = 0; i < a.size(); i++)
            {
                b.push_back(h);
            }
            for (int i = 0; i < a.size(); i++)
            {
                for (int j = 0; j < a.size(); j++)
                {
                    if (a[i][j] > 0)
                    {
                        m++;
                        b[i].insert(j+1);
                        b[j].insert(i+1);
                    }
                }
            }
            m = m/2;
            q23.push_back(i1);
            
            u = i1;
            v = i1;
            for (int i = 0; i < m; i++)
            {
                v = u;
                for (set<int>::iterator j = b[u-1].begin(); j != b[u-1].end(); j++)
                {
                    if (Most(b,u,(*j)) == 1)
                    {
                        u = (*j);
                        q23.push_back(u);
                        b[u-1].erase(v);
                        b[v-1].erase(u);
                        break;
                    }
                }
                if (v == u && b[u-1].size() > 0)
                {
                    u = (*b[u-1].begin());
                    q23.push_back(u);
                    b[u-1].erase(v);
                    b[v-1].erase(u);
                }
                
            }
        }
        if (indicator1 == 'L')
        {
            TransformToAdjList();
        }
        if (indicator1 == 'E')
        {
            TransformToListOfEdges();
        }
        
        return q23;
    }
    
    // Эффективный алгоритм поиска Эйлерова пути
    vector<int> GetEuleranTourEffective()
    {
        vector<set<int>> b;
        vector<int> q23;
        set<int> h;
        bool circleExist;
        int i1;
        stack <int> s;
        int u,v;
        char indicator1;
        indicator1 = indicator;
        TransformToAdjMatrix();
        q23.clear();
        i1 = CheckEuler(circleExist);
        m = 0;
        h.clear();
        for (int i = 0; i < a.size(); i++)
        {
            b.push_back(h);
        }
        for (int i = 0; i < a.size(); i++)
        {
            for (int j = 0; j < a.size(); j++)
            {
                if (a[i][j] > 0)
                {
                    m++;
                    b[i].insert(j+1);
                    b[j].insert(i+1);
                }
            }
        }
        u = i1;
        v = i1;
        s.push(u);
        while (s.empty() == 0)
        {
            v = s.top();
            if (b[v-1].empty() == 1)
            {
                q23.push_back(v);
                s.pop();
            }
            else
            {
                u = (*b[v-1].begin());
                s.push(u);
                b[v-1].erase(u);
                b[u-1].erase(v);
                
            }
            
        }
        if (indicator1 == 'L')
        {
            TransformToAdjList();
        }
        if (indicator1 == 'E')
        {
            TransformToListOfEdges();
        }
        return q23;
    }
    
    
    // алгоритм Крускала
    Graph GetSpaingTreeKruscal()
    {
        Dsu b = Dsu(n);
        Graph alpha = Graph(n);
        vector <tuple<int, int, int>> edges;
        tuple<int, int, int> q;
        int a1,a2,a3;
        int i1;
        char indicator1;
        indicator1 = indicator;
        TransformToAdjMatrix();
        for (int i = 0; i < n; i++)
        {
            for (int j = i; j < n; j++)
            {
                if (a[i][j] != 0)
                {
                    edges.push_back(make_tuple(i+1, j+1, a[i][j]));
                }
            }
        }
        SortVectorTuple(edges);
        
        i1=0;
        while( i1 < n-1 && edges.size() > 0)
        {
            tie(a1,a2,a3) = edges[edges.size()-1];
            edges.pop_back();
            if (b.Find(a1) != b.Find(a2))
            {
                i1++;
                alpha.AddEdge(a1,a2,a3);
                b.Union(a1,a2);
            }
        }
        if (indicator1 == 'L')
        {
            TransformToAdjList();
        }
        if (indicator1 == 'E')
        {
            TransformToListOfEdges();
        }
        return alpha;
    }
    
    
    // алгоритм Борувки
    Graph GetSpaingTreeBoruvka()
    {
        Dsu b=Dsu(n);
        Graph alpha=Graph(n);
        
        vector <set<tuple<int, int, int>>> rebrapodgrapha;
        vector <tuple<int, int, int>> naimensheerebro;
        set<tuple<int, int, int>> s1, s2;
        set<tuple<int, int, int>> s;
        tuple<int, int, int> u;
        int a1,a2,a3;
        int i1;
        int u1, u2;
        int q;
        char indicator1;
        indicator1=indicator;
        TransformToAdjList();
        for (int i=0;i<n;i++)
        {
            rebrapodgrapha.push_back(b2[i]);
        }
        i1=0;
        while (i1<n-1)
        {
            for (int i=0;i<n;i++)
            {
                if (rebrapodgrapha[i].empty()==0)
                {
                    MinTupleInSet(rebrapodgrapha[i],u);
                    naimensheerebro.push_back(u);
                }
            }
            SortVectorTuple(naimensheerebro);
            while(i1<n-1 && naimensheerebro.size()>0)
            {
                tie(a1,a2,a3)=naimensheerebro[naimensheerebro.size()-1];
                naimensheerebro.pop_back();
                if (b.Find(a1)!=b.Find(a2))
                {
                    i1++;
                    u1 = b.Find(a1);
                    u2 = b.Find(a2);
                    alpha.AddEdge(a1,a2,a3);
                    b.Union(a1,a2);
                    q = b.Find(u1);
                    s.clear();
                    s1.clear();
                    s2.clear();
                    s1 = rebrapodgrapha[u1-1];
                    s2 = rebrapodgrapha[u2-1];
                    rebrapodgrapha[u1-1].clear();
                    rebrapodgrapha[u2-1].clear();
                    for (set<tuple<int, int, int>>::iterator i = s1.begin(); i != s1.end(); i++)
                    {
                        tie(a1,a2,a3) = (*i);
                        if (b.Find(a1) != b.Find(a2))
                        {
                            s.insert((*i));
                        }
                    }
                    for (set<tuple<int, int, int>>::iterator i = s2.begin(); i != s2.end(); i++)
                    {
                        tie(a1,a2,a3) = (*i);
                        if (b.Find(a1) != b.Find(a2))
                        {
                            s.insert((*i));
                        }
                    }
                    rebrapodgrapha[q-1] = s;
                }
            }
        }
        if (indicator1 == 'C')
        {
            TransformToAdjMatrix();
        }
        if (indicator1 == 'E')
        {
            TransformToListOfEdges();
        }
        return alpha;
    }
};

// преобразование представления графа в матрицу смежности
void Graph::TransformToAdjMatrix()
{
    if (indicator == 'L')
    {
        if (w == 0)
            TransformListToMatrix1(b1, a);
        else
            TransformListToMatrix2(b2, a);
    }
    if (indicator == 'E')
    {
        m = 0;
        if (w == 0)
            TransformListOfEdgesToMatrix1(n, w1, a, d);
        else
            TransformListOfEdgesToMatrix2(n, w2, a, d);
    }
    if (indicator == 'L' || indicator == 'E')
        indicator = 'C';
}


// преобразование представления графа в список смежных вершин
void Graph::TransformToAdjList()
{
    if (indicator == 'C')
    {
        if (w == 0)
            TransformMatrixToList1(a, b1);
        else
            TransformMatrixToList2(a, b2);
    }
    if (indicator == 'E')
    {
        m = 0;
        if (w == 0)
            TransformListOfEdgesToList1(n, w1, b1);
        else
            TransformListOfEdgesToList2(n, w2, b2);
    }
    if (indicator == 'C' || indicator == 'E')
        indicator='L';
}

// преобразование представления графа в список ребер
void Graph::TransformToListOfEdges()
{
    if (indicator == 'C')
    {
        if (w == 0)
            TransformMatrixToListOfEdges1(m, a, w1);
        else
            TransformMatrixToListOfEdges2(m, a, w2);
    }
    if (indicator == 'L')
    {
        if (w == 0)
            TransformListToListOfEdges1(m, b1, w1);
        else
            TransformListToListOfEdges2(m, b2, w2);
    }

    if (indicator == 'C' || indicator == 'L')
        indicator = 'E';
}



// изменение  веса  ребра, функция возвращает  старое  значение  веса  ребра  для взвешенного графа, и функция  возвращает  0  для  невзвешенного  графа
int Graph::ChangeEdge(int from, int to, int newWeight)
{
    int weight;
    int a1, a2, a3;
    weight = 0;
    if (w == 1) //граф взвешенный
    {
        if (indicator == 'C')
        {
            weight = a[from-1][to-1];
            a[from-1][to-1] = newWeight;
            if (d == 0) // граф неориентированный
                a[to-1][from-1] = newWeight;
        }
        else
        {
            if (indicator == 'L')
            {
                for (set<tuple<int,int,int>>::iterator i = b2[from-1].begin(); i != b2[from-1].end(); i++)
                {
                    tie(a1, a2, a3) = (*i);
                    if (a1 == from && a2 == to)
                    {
                        weight = a3;
                        b2[from-1].erase(*i);
                        b2[from-1].insert(make_tuple(from, to, newWeight));
                        break;
                    }
                }
                if (d == 0) // граф неориентированный
                    for (set<tuple<int,int,int>>::iterator i = b2[to-1].begin(); i != b2[to-1].end(); i++)
                    {
                        tie(a1, a2, a3) = (*i);
                        if (a1 == to && a2 == from)
                        {
                            b2[to-1].erase(*i);
                            b2[to-1].insert(make_tuple(to, from, newWeight));
                            break;
                        }
                    }
            }
            else // indicator = e/E
            {
                for (set<tuple<int,int,int>>::iterator i = w2.begin(); i != w2.end(); i++)
                {
                    tie(a1, a2, a3) = (*i);
                    if (a1 == from && a2 == to)
                    {
                        weight = a3;
                        w2.erase(*i);
                        w2.insert(make_tuple(from, to, newWeight));
                        break;
                    }
                }
                if (d == 0) //граф неориентированный
                    for (set<tuple<int,int,int>>::iterator i = w2.begin(); i != w2.end(); i++)
                    {
		                tie(a1, a2, a3) = (*i);
                        if (a1 == to && a2 == from)
                        {
                            w2.erase(*i);
                            w2.insert(make_tuple(to, from, newWeight));
                            break;
                        }
                    }
            }
        }
    }
    return weight;
}


// удаление ребра
void Graph::RemoveEdge(int from, int to)
{
    int weight;
    int a1, a2, a3;
    if (indicator == 'C')
	   {
           a[from-1][to-1] = 0;
           if (d == 0) // граф неориентированный
           {
               a[to-1][from-1] = 0;
           }
       }
	   else
       {
           if (indicator == 'L')
           {
               if (w == 0) // граф невзвешенный
               {
                   b1[from-1].erase(make_pair(from, to));
                   if (d == 0) // граф неориентированный
                   {
                       b1[to-1].erase(make_pair(to, from));
                   }
               }
               else // граф взвешенный
               {
                   for (set<tuple<int,int,int>>::iterator i = b2[from-1].begin(); i != b2[from-1].end(); i++)
                   {
                        tie(a1, a2, a3) = (*i);
                        if (a1 == from && a2 == to)
                        {
                            weight = a3;
                            b2[from-1].erase(*i);
                            break;
                        }
                   }
                   if (d == 0) // граф неориентированный
                   {
                       for (set<tuple<int,int,int>>::iterator i = b2[to-1].begin(); i != b2[to-1].end(); i++)
                       {
                           tie(a1, a2, a3) = (*i);
                           if (a1 == to && a2 == from)
                           {
                               b2[to-1].erase(*i);
                               break;
                           }
                       }
                   }
               }
           }
           else // индикатор = Е
           {
               if (w == 0) // граф невзешенный
               {
                   w1.erase(make_pair(from, to));
                   if (d == 0) { // граф неориентированный
                       w1.erase(make_pair(to, from));
                       m--;
                   }
                   m--;
               }
               else // граф взвешенный ориентированный
               {
                   for (set<tuple<int,int,int>>::iterator i = w2.begin(); i != w2.end(); i++)
                   {
                        tie(a1, a2, a3) = (*i);
                        if (a1 == from && a2 == to)
                        {
                            weight = a3;
                            w2.erase(*i);
                            m--;
                            break;
                        }
                   }
                   if (d == 0) // граф взвешенный неориентированный
                       for (set<tuple<int,int,int>>::iterator i = w2.begin(); i != w2.end(); i++)
                       {
                           tie(a1, a2, a3) = (*i);
                           if (a1 == to && a2 == from)
                           {
                               w2.erase(*i);
                               m--;
                               break;
                           }
                       }
               }
           }
       }
}


// добавление нового ребра
void Graph::AddEdge(int from, int to,int weight)
{
    if (w == 0)
        weight = 1;
    if (indicator == 'C')
	   {
           a[from-1][to-1] = weight;
           if (d == 0)
               a[to-1][from-1] = weight;
       }
	   else
       {
           if (indicator == 'l' || indicator == 'L')
           {
               if (w == 0) //граф невзвшенный
               {
                   b1[from-1].insert(make_pair(from, to));
                   if (d == 0)
                       b1[to-1].insert(make_pair(to, from));
               }
               else  // граф взвешенный
               {
                   b2[from-1].insert(make_tuple(from, to, weight));
                   if (d == 0)
                       b2[to-1].insert(make_tuple(to, from, weight));
               }
           }
           else
           {
               if (w == 0) //граф невзвешенный
               {
                   w1.insert(make_pair(from, to));
                   m++;
                   if (d == 0) { // граф неориентированный
                       w1.insert(make_pair(to, from));
                       m++;
                   }
               }
               else // граф невзвешенный
               {
                   w2.insert(make_tuple(from, to, weight));
                   m++;
                   if (d == 0) { // граф неориентированный
                       w2.insert(make_tuple(to, from, weight));
                       m++;
                   }
               }
           }
       }
}


// запись графа в файл
void Graph::WriteGraph(string fileName)
{
    int a1, a2, a3;
    ofstream fout(fileName, ios::out);
    fout << indicator << " ";
    fout << n;
    if (indicator == 'E' && d == 0)
        fout << " " << m/2;
    else
        fout << " " << m;
    fout << endl;
    fout << d << " " << w << endl;
    if (indicator == 'C')
    {
        for (int i = 0; i < a.size(); i++)
        {
            //fout << " ";
            for (int j = 0; j < a[i].size(); j++)
                if (j != a[i].size() - 1)
                    fout << a[i][j] << " ";
                else
                    fout << a[i][j] << endl;
            //fout << endl;
        }
    }
    else
    {
        if (indicator == 'L')
        {
            if (w == 0)
            {
                for (int i = 0; i < b1.size(); i++)
                {
                    //fout << " ";
                    int k = 0;
                    for (set<pair<int,int>>::iterator j = b1[i].begin(); j != b1[i].end(); ++j, k++)
                        if (k == b1[i].size() - 1)
                            fout << (*j).second;
                        else
                            fout << (*j).second << " ";
                    fout << endl;
                }
            }
            else
            {
                for (int i = 0; i < b2.size(); i++)
                {
                    //fout << " ";
                    int k = 0;
                    for (set<tuple<int, int, int>>::iterator j = b2[i].begin(); j != b2[i].end(); j++, k++)
                    {
                        tie(a1, a2, a3) = (*j);
                        if (k == b2[i].size() - 1)
                            fout << a2 << " " << a3;
                        else
                            fout << a2 << " " << a3 << " ";
                    }
                    fout << endl;
                }
            }
        }
        else
        {
            if (w == 0)
            {
                for (set<pair<int,int>>::iterator i = w1.begin(); i != w1.end(); i++)
                    if ((d == 0 && (*i).first < (*i).second) || (d == 1)) {
                        fout << (*i).first << " " << (*i).second << endl;
                    }
            }
            else
                for (set<tuple<int,int,int>>::iterator i = w2.begin(); i != w2.end(); i++)
                {
                    tie(a1, a2, a3) = (*i);
                    if ((d == 0 && a1 < a2) || (d == 1)) {
                        fout << a1 << " " << a2 << " " << a3 << endl;
                    }
                }
            
        }
    }
    fout.close();
}


// чтение графа из файла
void Graph::ReadGraph(string fileName)
{
    ifstream fin;
    vector<string> st;
    string s, s1;
    int k;
    fin.open(fileName, ios::in);
    
    if (!fin)
        cout << "Ошибка, файл не найден " << endl;
    else
    {
        fin >> indicator >> n; // считывается индикатор и кол-во вершин в графе
        if (indicator == 'E')
            fin >> m; // считывается кол-во ребер в графе
        else
            m = 0;
        
        fin >> d >> w; // считывается индикатор на не/ориентированность и не/взвешенность
        if (indicator == 'C' || indicator == 'L' )
            k = n; // кол-во вершин
        else
            k = m; // кол-во ребер

        getline(fin, s1);
        for (int i = 0; i < k; i++)
        {
            getline(fin, s1); // считывается строка представления графа из файла
            st.push_back(s1);
        }
        fin.close();
        
        if (indicator == 'C')
            StringToMatrix(st, a); // заполняем матрицу смежности
        else
        {
            if (indicator == 'L')
            {
                if (w == 0)
                    StringToList1(st, b1); // заполняем список смежности для невзвешенного графа
                else
                    StringToList2(st, b2); // заполняем список смежности для взвешенного графа
            }
            else
            {
                if (w == 0)
                    StringToListOfEdges1(st, w1); // инициализируем список ребер для невзвешенного графа
                else
                    StringToListOfEdges2(st, w2); // инициализируем список ребер для взвешенного графа
            }
        }
    }
}

